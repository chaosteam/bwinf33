{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
module Graph where

import Control.Applicative
import Control.Monad
import Control.Monad.ST
import Data.Hashable

import qualified Data.HashTable.ST.Basic as Hash

-- | Representation of a graph with a start node.
data Graph edge node = Graph
  { rootNode   :: node                  -- ^ The start node
  , neighbours :: node -> [(edge, node)] -- ^ Return neighbours of the given node (edge is the label of the edge)
  }

-- | Do a breadth-first search, returning the path to the nearest node that
-- satisfies the predicate.
--
-- The result is either Nothing (which means that no node satisfying the predicate was
-- found) or Just a path, were the path consists of all the labels of the edges along
-- the shortest way from the root node to the node satisfying the predicate.
bfs :: forall edge node. (Eq node, Hashable node)
    => (node -> Bool)    -- ^ Predicate. True means the node is a solution.
    -> Graph edge node  -- ^ Graph to search
    -> Maybe [edge]     -- ^ Path to nearest node or Nothing if no satisfying node exists.
bfs solved Graph{..} = runST $ do
  table <- Hash.new
  Hash.insert table rootNode ()
  go table [([], rootNode)]
 where
  go :: Hash.HashTable s node () -> [([edge],node)] -> ST s (Maybe [edge])
  go _        []    = return Nothing
  go visited level = case solutions of
    ((path,_):_) -> return . Just $ reverse path
    []           -> nextLevel >>= go visited
   where
    solutions = filter (solved . snd) level
    nextLevel =
      fmap concat . forM level $ \(as,b) ->
      fmap concat . forM (neighbours b) $ \(a,b') -> do
        m <- Hash.lookup visited b'
        case m of
          Nothing -> [(a:as,b')] <$ Hash.insert visited b' ()
          Just _  -> return []
