using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task4
{
	//Class for holding and solving of alphametics
	class Alphametic
	{
		//a list of all varibles on the left side of the alphametic
		private List<Char> variables;

		//a list of all words on the left side, a word is an array of integers correspondig to
		// the indices in "variables"
		private List<int[]> leftWords;

		//the characters of the right word
		private Char[] rightWordChars;

		//the right word as an array of integers correspondig to the indices in "variables"
		private int[] rightWord;

		//list of operarators (+,-,*,/) in the order they appear
		private List<Char> operators;

		//Creats a new Alphametic from ist String representation
		public Alphametic(String str)
		{
			leftWords = new List<int[]>();
			operators = new List<Char>();
			variables = new List<Char>();

			//The first operator is a "+" to start with a 0 when evaluating the left side.
			operators.Add('+');

			//Remove all spaces
			str=str.Replace(" ", "");
			String[] split = str.Split('=');
			String[] leftSplit = split[0].Split('+', '-', '*', '/');

			for (int i = 0; i < split[0].Length; i++)
			{
				if (isOperator(split[0][i]))
				{
					operators.Add(split[0][i]);
				}
				else if (!variables.Contains(split[0][i]))
				{
					variables.Add(split[0][i]);
				}
			}

			foreach (String word in leftSplit)
			{
				leftWords.Add(convertCharArrayToVariables(word.ToCharArray()));
			}

			rightWordChars = split[1].ToCharArray();
			rightWord = convertCharArrayToVariables(rightWordChars);
			

			
		}

		//Returns if a character is an operator
		public Boolean isOperator(Char c)
		{
			return c.Equals('+') || c.Equals('-') || c.Equals('*') || c.Equals('/');
		}

		//Maps each character in "chars" to the correspondig index in "variables"
		public int[] convertCharArrayToVariables(Char[] chars)
		{
			int[] varNums = new int[chars.Length];
			for (int i = 0; i < chars.Length; i++)
			{
				varNums[i] = variables.IndexOf(chars[i]);
			}
			return varNums;
		}

		//Checks if "solution" a solution to the alphametic 
		//A possible solution is a list of integers which are the values for the correspondig
		//characters in "variables"
		public Boolean checkSolution(List<int> solution)
		{
			int leftSide = evaluateLeftSide(solution);
			if (leftSide < 0)
			{
				return false;	//The right side can't be negative
			}

			//characters and correspondig values that just appear in the right word are saved here
			List<Char> rightWordVariables = new List<Char>();  
			List<int> rightWordSolution = new List<int>();

			//Loops through all characters in the right word, from the last to the first
			for(int characterCount=rightWord.Length-1;characterCount>=0;characterCount--)
			{
				//Holds the digit the right word should be at this character
				int digit = leftSide % 10;

				//Checks if the character also appears on the left side, if so, it checks it's the
				//same as "digit"
				if (rightWord[characterCount] != -1 && digit != solution[rightWord[characterCount]])
				{
					return false;
				}

				//If the character only appears on the right side
				if (rightWord[characterCount] == -1)
				{
					//If the digit appears on the right side it can't be a solution, because equal
					// digits correspond to equal characters 
					if (solution.Contains(digit))
					{
						return false;
					}

					//If the digit also appears only on the right side, check if the
					// (character,digit) pair is consistent throughout the right word
					for (int i = 0; i < rightWordVariables.Count; i++)
					{
						if (rightWordVariables[i].Equals(rightWordChars[characterCount]) && rightWordSolution[i] != digit)
						{
							return false;
						}
						if (!rightWordVariables[i].Equals(rightWordChars[characterCount]) && rightWordSolution[i] == digit)
						{
							return false;
						}
					}

					//Both appeaed just oh the right side, so they get added to their correnspondig list
					rightWordVariables.Add(rightWordChars[characterCount]);
					rightWordSolution.Add(digit);
					
				}

				//Shift "leftSide" on digit to the right
				leftSide /= 10;
			}
			return true;
		}

		//Evaluates the Left side with the given possible solution
		public int evaluateLeftSide(List<int> solution)
		{
			//The first operator is '+', so we start with a 0
			int leftSide = 0;
			for (int i = 0; i < leftWords.Count; i++)
			{
				int j = i;

				//Evaluate * and / first
				int product = 1;
				while (j<leftWords.Count&&(j==i||operators[j].Equals("*") || operators[j].Equals("/")))
				{
					Char op = operators[j];
					if (j == i)
					{
						op = '*';
					}
					try
					{
						product = applyOperator(op, product, evaluateWord(leftWords[j], solution));
					}
					catch(DivideByZeroException){
						//In case the denominator is 0, we return -1, because negative left sides
						//can't be solutions anyway
						return -1;
					}
					j++;
				}

				//Apply + and - only after * and / have been applied
				try
				{
					leftSide = applyOperator(operators[i], leftSide, product);
				}
				catch(DivideByZeroException)
				{
					return -1;
				}
				i = j-1;
			}
			return leftSide;
		}

		//applies the operator "op" to "a" and "b" and returns the result
		public int applyOperator(Char op, int a, int b)
		{
			switch (op)
			{
				case '+':
					return a + b;
				case '-':
					return a - b;
				case '*':
					return a * b;
				case '/':
					if(a % b != 0) throw new DivideByZeroException(); // hack
					return a / b;
			}
			return -1;
		}

		//Evaluate a single word with the given possible solution
		public int evaluateWord(int[] word, List<int> solution)
		{
			int sum = 0;
			for (int i = 0; i < word.Length; i++)
			{
				sum = 10 * sum + solution[word[i]];
			}
			return sum;
		}

		//Solves the alphametic recursivly
		//"firstVals" contains values for the first characters from "variables" (important for recursion)
		public List<int> solve(List<int> firstVals)
		{
			if (firstVals.Count == variables.Count)
			{
				//If we already have all values given, just check if thats a solution
				if (checkSolution(firstVals))
				{
					//Add the variables from the right word
					return appendSolution(firstVals);
				}

				//null means that there is no solution
				return null;
			}

			
			for (int i = 0; i < 10; i++)
			{
				//If we don't have all values given yet, add a new possible value and solve in with
				//the new, longer given list

				//A value can't be used twice
				if (!firstVals.Contains(i))
				{
					List<int> startCopy = new List<int>(firstVals);
					startCopy.Add(i);
					List<int> sol=solve(startCopy);

					//just return, if we found a solution
					if (sol != null)
					{
						return sol;
					}
				}
			}
			return null;
		}

		//Solves the alphametic when no values are known
		public List<int> solve()
		{
			return solve(new List<int>());
		}

		//Returns a list of all variables (left and right side) in the alphametric
		public List<Char> getAllVariables()
		{
			List<Char> vars = new List<Char>(variables);
			foreach (Char c in rightWordChars)
			{
				if (!vars.Contains(c))
				{
					vars.Add(c);
				}
			}
			return vars;
		}

		// appends the solution by the values of all variables, that appear only in the right word
		public List<int> appendSolution(List<int> solution)
		{
			//Convert to a string to easiely get the decimal digits of the right side
			String s=evaluateLeftSide(solution).ToString();

			//Prepend 0s as long as "s" does not have the right length
			while (s.Length < rightWord.Length)
			{
				s = "0" + s;
			}

			for (int i = 0; i < s.Length; i++)
			{
				int digit=Convert.ToInt32(s.Substring(i,  1));

				//If digit is not in "solution" jet, it just appears on the right side, do it's added
				if (!(solution.Contains(digit)))
				{
					solution.Add(digit);
				}
			}
			return solution;
		   
		}

		//Prints the solution to the screen
		public void printSolution()
		{
			List<int> sol =solve();
			List<Char> vars = getAllVariables();
			if (sol == null)
			{
				Console.WriteLine("No Solution.");
			}
			else
			{
				Console.WriteLine("Solution:");
				for (int i = 0; i < vars.Count; i++)
				{
					Console.Write(vars[i] + "=" + sol[i]+" ");
				}
				Console.Write("\n");
			}
		}

	}

}
