using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task4
{
	class Program
	{
		//Used for findig number alphametics
		public static String[] numbers =
			{"EINS","ZWEI","DREI","VIER","FÜNF","SECHS","SIEBEN","ACHT","NEUN","ZEHN"};

		static void Main(string[] args)
		{
			while (true)  //Loop, so it's possible to solve multiple alphametics
			{
				String usage = "Please Enter";
				usage += "\n 1 - To solve an alphametic";
				usage += "\n 2 - To find number alphametics";
			    usage += "\n 3 - To quit";
				Console.WriteLine(usage);
				int choice = Convert.ToInt32(Console.ReadLine());

				if (choice == 1)
				{
					Console.WriteLine("Enter an Alphametic:");
					String alphametic = Console.ReadLine();
					Alphametic alpha = new Alphametic(alphametic);

					alpha.printSolution();
				}

				else if (choice == 2)
				{
					Console.WriteLine("Enter the maximum number of summands:");
					int maxSummands = Convert.ToInt32(Console.ReadLine());
					findAlphametics(maxSummands);
				}

				else if (choice == 3)
				{
					break;
				}
			}
		}

		//finds alphametics with at most "maxCount" summands
		public static void findAlphametics(int maxCount)
		{
			findAlphametics(new List<int>(), maxCount);
		}

		//finds alphametics with at most "maxCount" summands that contain "nums" as the
		//smallest numbers
		public static void findAlphametics(List<int> nums, int maxCount)
		{
			//If we have an actual equation (more than one summand), check if it's a true number 
			//alphametic too
			if (nums.Count >1)
			{
				checkForAlphametic(nums);
			}

			if (nums.Count < maxCount)
			{
				int min = 1;
				int max = 10;
				if (nums.Count > 0)
				{
					//To avoid duplicated alphametics, we order the summands,
					//so the new one needs to be greater or equal the last one
					min = nums[nums.Count - 1];	
				}

				//The sum should be less or equal to ten
				foreach (int i in nums)
				{
					max -= i;	
				}

				//For each possible new summand find alphametics with it
				for (int i = min; i <= max; i++)
				{
					List<int> numCopy = new List<int>(nums);
					numCopy.Add(i);
					findAlphametics(numCopy, maxCount);
				}
			}
		}

		//Checks if the numbers in nums as summands form a number alphametic
		public static void checkForAlphametic(List<int> nums)
		{
			//Creates the string representation of the alphametic
			String str = "";
			int sum = 0;
			foreach (int i in nums)
			{
				str += numbers[i-1]+"+";
				sum += i;
			}
			str = str.Substring(0, str.Length - 1) + "=" + numbers[sum-1];

			Alphametic alphametic = new Alphametic(str);

			//If it has a solution it is printed
			if (alphametic.solve() != null)
			{
				Console.WriteLine(str);
				alphametic.printSolution();
				Console.WriteLine("=========================================");
			}
		}
	}
}
