module Main where

iterateCollectFirst :: ((a, b) -> (a, b)) -> (a, b) -> [a]
iterateCollectFirst f = map fst . iterate f

dropWhileZip :: [a -> Bool] -> [a] -> [a]
dropWhileZip _        []                   = []
dropWhileZip []       xs                   = xs
dropWhileZip (f : fs) (x : xs) | f x       = dropWhileZip fs xs
                               | otherwise = x : xs

next :: Integer -> (Integer, Integer) -> (Integer, Integer)
next loopLength (distanceFromEnd, count) = (newDistanceFromEnd, count - reduction) where
  (reduction, newDistanceFromStart) = quotRem (loopLength - distanceFromEnd + count) loopLength
  newDistanceFromEnd = if newDistanceFromStart > 0 then loopLength - newDistanceFromStart else 0

solve :: Integer -> Integer -> Integer -> Integer
solve loopLength extra count = findFirstRoundWon allRounds where
  findFirstRoundWon = head . dropWhileZip [ (> extra * x) | x <- [ 1 .. ] ]
  allRounds = iterateCollectFirst (next loopLength) (loopLength - 1, count)

main :: IO ()
main = do
  putStrLn "How many people are waiting:"
  n <- fmap read getLine
  if n < 1
     then putStrLn "No people waiting. No problem to solve!"
     else case solve 16 1 n of
            0 -> putStrLn "Say no extra syllabels."
            1 -> putStrLn "Say an extra syllabel on your first turn."
            x -> putStrLn $ "Say an extra syllabel on your first " ++ show x ++ " turns."
