{ haskellPackages ? pkgs.haskellPackages
, pkgs ? import <nixpkgs> {} }: with pkgs;

let
  buildHS = name: main: srcs: depsFun: stdenv.mkDerivation {
    inherit name;
    buildInputs = [(haskellPackages.ghcWithPackages depsFun)];
    phases = ["unpackPhase" "installPhase" "fixupPhase"];
    outputs = ["out" "submission"];
    unpackPhase = ''
      mkdir src
      cd src
      ${pkgs.lib.concatMapStringsSep "\n" (src: ''
        cp ${src} ${pkgs.lib.concatStringsSep "-" (builtins.tail (pkgs.lib.splitString "-" src))}
      '') ([main] ++ srcs)}
    '';
    installPhase = ''
      mkdir -p $submission
      cp -r . $submission/${name}
      ghc -o $out ${main}
      cp $out $submission/${name}/main
      chmod +x $submission/${name}/main
    '';
  };

  task1 = buildHS "task1" ./Task1.hs [./Graph.hs] (hs: with hs; [hashtables hashable deepseq vector]);
  task2 = buildHS "task2" ./Task2.hs [] (hs: with hs; [free lens boxes zippers semigroups]);
  task3 = buildHS "task3" ./Task3.hs [] (_: []);

  task4 = stdenv.mkDerivation {
    name = "task4";
    src = ./task4;
    phases = ["unpackPhase" "installPhase" "fixupPhase"];
    buildInputs = [ pkgs.mono ];
    outputs = ["out" "exe" "submission"];
    installPhase = ''
      cd $src
      mcs -out:$exe ./Alphametic.cs ./Program.cs
      cat > $out <<EOF
      #!${pkgs.bash}/bin/bash
      ${pkgs.mono}/bin/mono $exe "$@"
      EOF
      chmod +x $out
      mkdir -p $submission
      cp -r . $submission/task4
      chmod +w $submission/task4
      cp $exe $submission/task4/Task4.exe
    '';
  };

  doc = pkgs.stdenv.mkDerivation rec {
    name = "doc";
    latex = pkgs.texLiveAggregationFun {
      paths = with pkgs; [ texLive texLiveExtra ];
    };
    buildInputs = with pkgs; [ pythonPackages.pygments which latex rubber ];
    phases = ["unpackPhase" "buildPhase" "installPhase" "fixupPhase"];
    outputs = ["out" "submission"];
    src = ./.;
    buildPhase = ''
      latexmk -pdf -pdflatex='pdflatex -file-line-error --shell-escape -synctex=1' ./main.tex -jobname=main
      echo ""
      echo ""
      echo "-- error summary -----------------------------------------------"
      rubber-info main.log
      echo "----------------------------------------------------------------"
      echo ""
    '';
    installPhase = ''
      cp main.pdf $out
      mkdir -p $submission
      cp main.pdf $submission/dokumentation.pdf
    '';
  };
  targets = [ task1 task2 task3 task4 doc ];
in {
  all = (stdenv.mkDerivation {
    name = "all";
    outputs = map (x: x.name) targets;
    phases = ["installPhase"];
    installPhase = ''
      ${pkgs.lib.concatMapStringsSep "\n" (x: ''
        cp ${x.out} ''${${x.name}}
      '') targets}
    '';
  }).all;
  submission = stdenv.mkDerivation {
    name = "submission";
    phases = ["installPhase"];
    buildInputs = [ pkgs.zip ];
    tree = pkgs.buildEnv {
      name = "archive";
      paths = map (x: x.submission) targets;
    };
    installPhase = ''
      builddir=$PWD
      cd $tree; zip -r $builddir/z.zip . # */
      cd $builddir
      mv z.zip $out
    '';
  };
}
