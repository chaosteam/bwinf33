module Main where

import Criterion.Main
import Solver

eitherErr :: Either String a -> a
eitherErr = either error id

problem1 :: Problem
problem1 = eitherErr $ problem 1 [8, 5, 3] [8, 0, 0]

problem2 :: Problem
problem2 = eitherErr $ problem 2 [10, 8, 11, 7] [10, 4, 4, 6]

problem3 :: Problem
problem3 = eitherErr $ problem 2 [6, 26, 13, 50] [0, 0, 0, 20]

problem4 :: Problem
problem4 = eitherErr $ problem 2 [6, 26, 13, 50, 20, 14] [0, 0, 0, 20, 10, 12]

problem5 :: Problem
problem5 = eitherErr $ problem 4 [4, 4, 4, 2, 8, 6, 9, 10] [3, 2, 1, 1, 4, 3, 6, 8]

main :: IO ()
main = defaultMain
  [ bench "example1" $ nf solve problem1
  , bench "example2" $ nf solve problem2
  , bench "example3" $ nf solve problem3
  , bench "example4" $ nf solve problem4
  , bench "example5" $ nf solve problem5
  ]
