#include <iostream>
#include <unordered_set>
#include <vector>
#include <queue>
#include <algorithm>
#include <iterator>
#include <boost/optional.hpp>
#include <boost/functional/hash.hpp>

struct step {
  std::size_t from, to;
};

struct problem;

struct solution {
  problem const* problem;
  std::vector<unsigned int> volumes;
  std::vector<step> steps;

  bool fair() const;

  void visualize() const {
    for(auto&& step : steps) {
      std::cout << step.from << " -> " << step.to << "\n";
    }
  }

  void transfer(std::size_t from, std::size_t to);

  bool operator==(solution const& other) const {
    return other.volumes == volumes;
  }
};

template <> struct std::hash<solution> {
  std::size_t operator()(solution const& sol) const {
    return boost::hash_value(sol.volumes);
  }
};

struct problem {
  friend std::istream& operator>>(std::istream& in, problem& prob);
  boost::optional<solution> solve() const;
  int firstCount;
  std::vector<unsigned int> init_volumes;
  std::vector<unsigned int> capacities;
};

std::istream& operator>>(std::istream& in, problem& prob) {
  in >> prob.firstCount >> std::ws;
  std::string line;
  std::getline(in, line);
  {
    std::stringstream stream(line);
    std::istream_iterator<unsigned int> it(stream), end;
    std::copy(it, end, std::back_inserter(prob.capacities));
  }
  std::getline(in, line);
  {
    std::stringstream stream(line);
    std::istream_iterator<unsigned int> it(stream), end;
    std::copy(it, end, std::back_inserter(prob.init_volumes));
  }
  return in;
}

boost::optional<solution> problem::solve() const {
  std::queue<solution> nodes;
  std::unordered_set<solution> visited;
  nodes.push(solution{this, init_volumes, {}});
  visited.insert(nodes.front());
  unsigned int processed = 0;
  while(!nodes.empty()) {
    std::cout << "\rProcessed " << ++processed << " nodes [" << nodes.size() << " queued]"  << std::flush;
    solution cur = nodes.front();
    nodes.pop();
    if(cur.fair()) {
      std::cout << "\n";
      return cur;
    }
    for(std::size_t from = 0; from < cur.volumes.size(); ++from) {
      for(std::size_t to = 0; to < cur.volumes.size(); ++to) {
	if(from == to) continue;
	solution next = cur;
	next.transfer(from, to);
	auto res = visited.insert(next);
	if(res.second) nodes.push(next);
      }
    }
  }
  std::cout << "\n";
  return boost::none;
}

void solution::transfer(std::size_t from, std::size_t to) {
  int transferVolume = std::min(volumes[from], problem->capacities[to] - volumes[to]);
  volumes[from] -= transferVolume;
  volumes[to] += transferVolume;
  steps.push_back(step{from, to});
}

bool solution::fair() const {
  return
    std::accumulate(volumes.begin(), volumes.begin() + problem->firstCount, 0)
    == std::accumulate(volumes.begin() + problem->firstCount, volumes.end(), 0);
}


template <typename T> T read_from(std::istream& in) {
  T t;
  in >> t;
  return t;
}

int main() {
  problem prob = read_from<problem>(std::cin);
  if (auto solution = prob.solve()) {
    solution->visualize();
  } else {
    std::cout << "No solution found." << std::endl;
    return 1;
  }
}
