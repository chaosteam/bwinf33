{-# LANGUAGE BangPatterns #-}

import Control.DeepSeq
import Control.Exception (evaluate)
import Control.Monad
import Solver
import System.CPUTime
import System.IO
import System.Random
import System.Timeout

bench :: Problem -> IO Integer
bench x = do
  start <- getCPUTime
  void $ timeout 20000000 $ evaluate $ force $ solve x
  end <- getCPUTime
  return $ end - start

untilRight :: IO (Either a b) -> IO b
untilRight act = do
  a <- act
  case a of
    Left _ -> untilRight act
    Right r -> return r

genProblem :: IO Problem
genProblem = do
  size <- randomRIO (4,20)
  first <- randomRIO (1, size `quot` 2)
  untilRight $ do
    caps <- replicateM size $ randomRIO (1, 20)
    ols <- mapM (randomRIO . (,) 0) (drop 1 caps)
    v   <- randomRIO (1, head caps `quot` 2 - 1)
    let vols = if odd (sum ols) then v * 2 + 1 : ols else v * 2 : ols
    return $ problem first caps vols

main :: IO ()
main = hSetBuffering stdout NoBuffering >> go 0 where
  go !best = do
    x <- genProblem
    new <- bench x
    when (new > best) $ putStrLn $ "\nNew best: " ++ show x ++ "[" ++ show (fromIntegral new / 1e12 :: Double) ++ "]"
    putStr "."
    go (max new best)
