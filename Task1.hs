{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Graph

import Control.Applicative
import Data.List
import System.Exit
import System.IO
import Control.DeepSeq
import Data.Hashable
import Data.Vector.Unboxed (Vector)

import qualified Data.Vector.Unboxed as Vector

-- | A cup is represented as an ID between 0 and @numCups - 1@.
type Cup = Int

-- | The 'Problem' data type describes the situation at the beginning and also
-- includes data that doesn't change when we pour water from one cup to another.
data Problem = Problem
  { allCups      :: [Cup]
  , numFirstCups :: Int
  , maxVolume    :: Volumes
  , initVolumes  :: Volumes
  } deriving (Eq, Show)

-- | Make a new problem given a number of cups, the number of cups of the first person
-- and a list of maximum/initial volumes of the cups.
-- Returns @Left err@ if the constructed problem is known to be invalid or unsolvable.
problem :: Int -> [Int] -> [Int] -> Either String Problem
problem nf mv iv
  | odd total = Left "Sum of all inital volumes must be even for a fair distribution."
  | length iv /= length mv = Left "Number of maximum and initial volumes must be equal."
  | sum maxA < half || sum maxB < half = Left "Maximum volumes not high enough to allow fair distribution."
  | or $ zipWith (>) iv mv = Left "One of the cups has a greater initial volume than it's maximum volume allows."
  | otherwise = Right $ Problem [0..nc-1] nf (Vector.fromList mv) . Vector.fromList $ iv
 where
  nc = length mv
  total = sum iv
  half = total `quot` 2
  (maxA, maxB) = splitAt nf mv

-- | This data type saves the volume of each cup.
type Volumes = Vector Int

instance Hashable (Vector Int) where
  hashWithSalt = Vector.foldl' hashWithSalt
  {-# SPECIALIZE instance Hashable (Vector Int) #-}

-- | A 'Step' is one action that we can perform, pouring water from one cup
-- to another.
data Step = Pour Cup Cup deriving (Eq, Show)
instance NFData Step where
  rnf (Pour a b) = rnf a `seq` rnf b

-- | A solution is a list of Steps that lead to a fair distribution of water.
type Solution = [Step]

-- | Perform a step, calculating the new volumes of the affected cups.
performStep :: Vector Int      -- ^ maximum values of cups
            -> Step            -- ^ step to perform
            -> Volumes         -- ^ current volumes of all cups
            -> (Step, Volumes) -- ^ action performed and new volumes of all cups
performStep maxVol s@(Pour f t) volumes
  = (s, volumes Vector.// [(f, fromVolume'), (t, toVolume')])
 where
  fromVolume  = volumes Vector.! f
  toVolume    = volumes Vector.! t
  pourVolume  = min fromVolume (maxVol Vector.! t - toVolume)
  toVolume'   = toVolume + pourVolume
  fromVolume' = fromVolume - pourVolume

-- | Check if the distribution of water is fair.
fair :: Int     -- ^ Number of cups of the first person
     -> Volumes -- ^ Current volumes of each cup
     -> Bool    -- ^ True when fair
fair firstCount vols = Vector.sum volumesA == Vector.sum volumesB where
  (volumesA, volumesB) = Vector.splitAt firstCount vols

-- | Find a solution for the given problem. If no solution is possible,
-- return Nothing, otherwise return Just the solution.
solve :: Problem -> Maybe Solution
solve Problem{..} = bfs (fair numFirstCups) $ Graph initVolumes next where
  next = flip map allSteps . flip (performStep maxVolume)
  allSteps = [Pour f t | f <- allCups, t <- allCups, f /= t]

--------------------------------------------------------------------------------
-- | Print an error message and exit.
fatal :: String -> IO a
fatal err = hPutStrLn stderr ("Error: " ++ err) >> exitFailure

-- | Read a problem from stdin.
readProblem :: IO Problem
readProblem = do
  cupsACount <- readLn
  mvolumes <- map read . words <$> getLine
  volumes  <- map read . words <$> getLine
  either fatal return $ problem cupsACount mvolumes volumes

-- | Pretty-print the current volumes of all the cups.
showVolumes :: Problem -> Volumes -> String
showVolumes Problem{..} volumes = unwords [showVolume i | i <- allCups]
 where showVolume i =
         "[" ++ show (volumes Vector.! i) ++ "/" ++ show (maxVolume Vector.! i) ++ "]"

-- | Pretty-print a Step.
showStep :: Step -> String
showStep (Pour source target) = show source ++ " -> " ++ show target

-- | Pretty-print the solution.
showSolution :: Problem -> Solution -> String
showSolution p@Problem{..}
  = unlines . (showVolumes p initVolumes :) . snd . mapAccumL f initVolumes
 where f volumes step = (volumes', showVolumes p volumes' ++ " | " ++ showStep step)
         where volumes' = snd $ performStep maxVolume step volumes

main :: IO ()
main = do
  prob <- readProblem
  putStr . maybe "No solution.\n" (showSolution prob) . solve $ prob
