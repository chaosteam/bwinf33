{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Main where

import Control.Applicative
import Control.Arrow hiding ( (<+>), left, right )
import Control.Lens hiding ( (<|) )
import Control.Monad
import Control.Monad.Free
import Control.Monad.State
import Control.Zipper
import Data.List
import Data.List.NonEmpty ( NonEmpty(..), nonEmpty, (<|), toList )
import Data.Maybe
import Data.Monoid hiding ( (<>) )
import Data.Ord
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Tuple
import Text.PrettyPrint.Boxes hiding ( moveRight, moveLeft )

newtype Position = Position { position :: Integer } deriving (Show, Eq, Ord, Enum, Num, Real, Integral)
newtype Weight = Weight { weight :: Integer } deriving (Show, Eq, Ord, Enum, Num, Real, Integral)
newtype Torque = Torque { torque :: Integer } deriving (Show, Eq, Ord, Enum, Num, Real, Integral)

newtype Bar a = Bar { hangings :: [(Position, a)] } deriving (Show, Functor)
newtype Mobile = Mobile { mobile :: Free Bar Weight } deriving Show

-- | Check that the mobile is balanced.
check :: Mobile -> Bool
check = isJust . check' . mobile where
  check' :: Free Bar Weight -> Maybe (Sum Weight)
  check' (Pure x)        = Just $ Sum x
  check' (Free (Bar xs)) = mconcat <$> do
      xs' <- traverse (traverse check') xs
      guard $ (== 0) $ sum $ uncurry (*) . (position *** weight . getSum) <$> xs'
      return $ view _2 <$> xs'

-- | Create a tree-like structure showing all important features of the tree.
pretty :: Mobile -> Box
pretty = iter (buildBranch . map annotate . hangings) . fmap buildLeaf . mobile where
  buildLeaf :: Weight -> Box
  buildLeaf (Weight w) = "+" // "|" // ("*" <+> text (show w))

  annotate :: (Position, Box) -> Box
  annotate (Position p, box) = annotation // (char '-' <> box) where
    annotation | p == 0     = text " 0"
               | abs p == p = text $ '+' : show p
               | otherwise = text (show p)

  buildBranch :: [Box] -> Box
  buildBranch xs = "+" // ("|" // foldr (//) "+-" (map (line . rows) $ init xs) <> vsep 1 left xs)

  line :: Int -> Box
  line n = "+-" // vcat left (replicate n "|")

-- | Create a mobile with the given weights.
-- The whole number specifies the maximal number of hangings at one bar.
create :: Integral i => i -> NonEmpty Weight -> Mobile
create n = Mobile . view _2 . balance . tree where
  tree :: NonEmpty a -> Free [] a
  tree (x :| []) = Pure x
  tree xs        = Free $ tree <$> toList (distributeOn n xs)
 
  balance :: Free [] Weight -> (Weight, Free Bar Weight)
  balance (Pure x)  = (x, Pure x)
  balance (Free xs) = (sum $ view _1 <$> xs', Free $ Bar $ uncurry (uncurry cancelTorque) $ minimizeTorque xs')
    where xs' = balance <$> xs

  cancelTorque :: Torque -> Set Position -> [(Position, Weight, a)] -> [(Position, a)]
  cancelTorque 0 _ xs = (view _1 &&& view _3) <$> xs
  cancelTorque t s xs = go $ fromWithin traverse $ zipper xs where
    t' = torque t
    go z | p' `Set.notMember` s' = rezip z & traverse %~ (view _1 &&& view _3) & traverse._1 *~ factor & element (tooth z)._1 +~ p''
         | otherwise             = go $ tug rightward z
      where (p, w, _) = z^.focus
            t''       = position p * weight w
            multiple  = lcm t' t''
            factor    = Position $ multiple `quot` t'
            factor'   = Position $ multiple `quot` t''
            s'        = Set.mapMonotonic (* factor) s
            p'        = p * (factor + factor')
            p''       = p * factor'

  minimizeTorque :: [(Weight, a)] -> ((Torque, Set Position), [(Position, Weight, a)])
  minimizeTorque = swap . flip runState (0, Set.singleton 0) . traverse go . sortBy (flip $ comparing $ view $ _1 . to weight) where
    go :: (Weight, a) -> State (Torque, Set Position) (Position, Weight, a)                                            
    go (w, x) = do
       (t, s) <- get
       let p  = head $ dropWhile (`Set.member` s) $ near $ Position $ minimizeRemainder (torque t) (weight w)
       (p, w, x) <$ put (t - Torque (weight w * position p), Set.insert p s)

-- | Distribute the elments of the list on a whole number of lists.
-- Every list will contain at most one element more than the shortest list.
-- Every element of the orginal list will be in exactly one ot the generated lists,
-- but the order of the elements will change.
distributeOn :: forall a i . Integral i => i -> NonEmpty a -> NonEmpty (NonEmpty a)
distributeOn n (x' :| xs') | n < 1     = error "distributeOn: non-positive number of lists to distribute on"
                           | otherwise = distribute 1 xs' $ (x' :| []) :| [] where

  distribute :: i -> [a] -> NonEmpty (NonEmpty a) -> NonEmpty (NonEmpty a)
  distribute _ []       xss             = xss
  distribute i (x : xs) xss | i < n     = distribute (i + 1) xs $ (x :| []) <| xss
                            | otherwise = distribute' True (x : xs) $ fromWithin traverse $ zipper xss

  distribute' :: Bool -> [a] -> Top :>> NonEmpty (NonEmpty a) :>> NonEmpty a -> NonEmpty (NonEmpty a)
  distribute' _         []       z = rezip z
  distribute' moveRight (x : xs) z = maybe (distribute' (not moveRight) xs z') (distribute' moveRight xs) $
                                     (if moveRight then rightward else leftward) z' where z' = z & focus %~ (x <|)

-- | Return the whole number for which |x - y * minimizeRemainder x y| is minimal.
minimizeRemainder :: Integral i => i -> i -> i
minimizeRemainder x y = round (fromIntegral x / fromIntegral y :: Float) -- TODO: better implementation?

-- | Return an infinite list of distinct whole numbers alternating around the input.
-- 'near x' is equivalent to 'map (+ x) [0, 1, -1, 2, -2, 3, -3, ...]'.
near :: Integral i => i -> [i]
near a = a : foldr (\ x xs -> a + x : a - x : xs) [] [1 .. ]

main :: IO ()
main = do
  putStrLn "Enter the weights of the figures seperated by spaces:"
  weights <- map (Weight . read) . words <$> getLine
  unless (all (> 0) weights) $ fail "Cannot generate a mobile. The weights must be positive!"
  putStrLn "This is the generated mobile:"
  printBox $ pretty $ create (4 :: Integer)
             $ fromMaybe (fail "Cannot generate a mobile. No weights given!") $ nonEmpty weights
